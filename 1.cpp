#define _CRT_SECURE_NO_WARNINGS
#include <random>
#include <iostream>


using namespace std;

int  main() {
    setlocale(LC_ALL, "");
    // объявляем переменные
    int x, y, polusumma, udvoenniе;
    cout << "Введите 2 числаs\n";
    cin >> x >> y;
    //вычисляем полусумму и удвоенное произведение
    polusumma = (x + y) / 2;
    udvoenniе = x * y * 2;
    //если введённые данные равны, то ошибка
    if (x == y) {
        cout << "Ошибка: x равен y\n";
        return 1;
    }
    // если x максимальное(x > y) присвоить x удвоенное произведение, а в y полусумму
    if (x > y) {
        cout << "Максимальное число - это x = " << x << endl;
        cout << "Минимальное число - это y = " << y << endl;
        x = udvoenniе;
        y = polusumma;
        cout << "x после вычисления " << x << endl;
        cout << "y после вычисления " << y << endl;
    }
    else {
        cout << "Максимальное число - это y = " << y << endl;
        cout << "Минимальное число - это x = " << x << endl;
        x = polusumma;
        y = udvoenniе;
        cout << "x после вычисления " << x << endl;
        cout << "y после вычисления " << y << endl;
    };
}



