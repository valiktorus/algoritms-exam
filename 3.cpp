#define _CRT_SECURE_NO_WARNINGS
#include <random>
#include <iostream>
#include <string.h>


using namespace std;

bool prepinanie(char ch) {
    //создание массива символов со знаками препинания
    char znakiPrepinan[] = ".,:;";
    //определяю есть ли символ в строке znakiPrepinan. strchr возвращает NULL если символа там нету
    char* a = strchr(znakiPrepinan, ch);
    //если символа нету в знаках препинания, т.е == NULL, вернёт false
    if (a == NULL) {
        return false;
    }
    else {
        return true;
    }
}

int  main() {
    setlocale(LC_ALL, "");
    //объявляю строку для записи с консоли
    char stroka[256];
    cout << "Введите строку для записи в файл со знаками препинания\n";
    //считываем из консоли в переменную
    cin >> stroka;
    //открываем файл D:\\file.txt
    FILE* f1;
    f1 = fopen("D:\\file.txt", "w+");
    //если ошибка, то программа завершится
    if (f1 == NULL) {
        return 1;
    }
    // записываем строку в файл
    fputs(stroka, f1);
    //закрываем файл
    fclose(f1);
    //открываем опять файл для чтения
    f1 = fopen("D:\\file.txt", "r");
    cout << "Знаки препинания\n";
    //пока не конец файла
    while (!feof(f1)) {
        //считать один символ из файла
        char ch = fgetc(f1);
        //проверяю, если символ является знаком препинания, то вывожу его
        if (prepinanie(ch)) {
            cout << ch;
        }
    }



}



