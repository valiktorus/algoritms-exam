#define _CRT_SECURE_NO_WARNINGS
#include <random>
#include <math.h>
#include <iostream>

using namespace std;

void fillInArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; ++i) {
        //заполняем числами случайными до 10
        array[i] = rand() % 10;
    }
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; ++i) {
        cout << array[i] << " ";
    }
    cout << endl;
}

int kolichestvoBolshe3(int array[], int arraySize){
    //счётчик количества элементов <=3
    int count = 0;
    //проверяем каждый элемент. если <= 3, то увеличиваем счётчик
    for (int i = 0; i < arraySize; ++i) {
        if(array[i] <= 3){
            count++;
        }
    }
    return count;
}

void fillInArray3(int array1[], int array2[], int arraySize, int array3[]){
    //делаем счётчик для 3го массива, для отсчёта индексов
    int a = 0;
    //пробегаемся по первому массиву. Если элемент <= 3 записываем его в 3й массив
    for (int i = 0; i < arraySize; ++i) {
        if(array1[i] <= 3){
            //если число в первом массиве <= 3 записываем его в 3 с индексом 3
            array3[a] = array1[i];
            //увеличиваем индекс в 3м массиве
            a++;
        }
    }
    //пробегаемся по второму массиву. Если элемент <= 3 записываем его в 3й массив
    for (int i = 0; i < arraySize; ++i) {
        if(array2[i] <= 3){
            array3[a] = array2[i];
            a++;
        }
    }
}



int main() {
    setlocale(LC_ALL, "");
    //объявляем переменную размера 2х массивов
    int arraySize;
    cout << "Введите размер массивов\n";
    //заносим размер массивов в переменную arraySize
    cin >> arraySize;
    //объявляем переменные двух исходных массивов
    //!!!!!! динамическая инициализация
    int *array1 = new int[arraySize];
    int	*array2 = new int[arraySize];
    //заполняем два массива в отдельной функции
    fillInArray(array1, arraySize);
    fillInArray(array2, arraySize);
    //вывожу массивов первых
    cout << "первый массив\n";
    printArray(array1, arraySize);
    cout << "второй массив\n";
    printArray(array2, arraySize);
    //считаем сколько в перевом массиве чисел <=3
    int kolichestvoVPervom = kolichestvoBolshe3(array1, arraySize);
    //считаем сколько во втором массиве чисел <=3
    int kolichestvoVoVtorom = kolichestvoBolshe3(array2, arraySize);
    //количество чисел <= 3 в 2х массивах (это будет размер 3го массива)
    //!!!!!! опечатка была тут
    const int kolichestvoObchee = kolichestvoVPervom + kolichestvoVoVtorom;
    //   объявляем 3й массив
    //!!!!!! динамическая инициализация, иначе константу нужно
    int *array3 = new int[kolichestvoObchee];
    //заполняем 3й массив
    fillInArray3(array1, array2, arraySize, array3);
    //выводим третий массив
    cout << "Третий массив\n";
    printArray(array3, kolichestvoObchee);
}



